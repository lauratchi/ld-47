﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public Transform player;

    [Space(10)]
    public Transform eyePivot;
    public float eyeSpeed = 2f;

    [Space(10)]
    public BossBullet bulletPrefab;
    public Transform bulletsParent;
    public float emissionRadius = 1f;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            EmitWave();
        }
    }

    private void LateUpdate()
    {
        Vector3 eyeToPlayer = player.position - eyePivot.position;
        float rotation = Mathf.Atan2(eyeToPlayer.y, eyeToPlayer.x) * Mathf.Rad2Deg + 90f;

        Quaternion targetRotation = Quaternion.Euler(0.0f, 0.0f, rotation);

        eyePivot.rotation = Quaternion.Lerp(eyePivot.rotation, targetRotation, Time.deltaTime * eyeSpeed);
    }

    private int bulletsAmount = 3;
    private float angleOffset = 30f;

    private void EmitWave()
    {
        Vector3 bossToPlayer = transform.position - player.position;
        float targetAngle = Mathf.Atan2(bossToPlayer.y, bossToPlayer.x) * Mathf.Rad2Deg;

        float startAngle = targetAngle - angleOffset;
        float endAngle = targetAngle + angleOffset;
        float angleStep = (endAngle - startAngle) / bulletsAmount;
        float angle = startAngle;

        for (int i = 0; i < bulletsAmount; i++)
        {
            float dirX = transform.position.x + Mathf.Sin(angle * Mathf.Deg2Rad);
            float dirY = transform.position.y + Mathf.Cos(angle * Mathf.Deg2Rad);

            Vector3 dir = new Vector3(dirX, dirY, 0f) - transform.position;
            dir.Normalize();

            BossBullet bullet = Instantiate(bulletPrefab, bulletsParent);
            bullet.Setup(transform.position + dir * emissionRadius, dir, 2f);

            angle += angleStep;
        }
    }

    public void OnDrawGizmos()
    {
        int points = 8;
        float angleStep = 360f / points;
        float angle = 0f;

        for (int i = 0; i < points; i++)
        {
            float dirX = transform.position.x + Mathf.Sin(angle * Mathf.Deg2Rad);
            float dirY = transform.position.y + Mathf.Cos(angle * Mathf.Deg2Rad);

            Vector3 dir = new Vector3(dirX, dirY, 0f) - transform.position;
            dir.Normalize();

            Gizmos.DrawSphere(transform.position + dir * emissionRadius, 0.1f);

            angle += angleStep;
        }
    }
}
