﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullet : MonoBehaviour
{
    private Vector3 direction;
    private float speed;

    public void Setup(Vector3 pos, Vector3 dir, float s)
    {
        transform.position = pos;
        direction = dir;
        speed = s;

        Invoke("Destroy", 3f);
    }

    private void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }

    public void PlayerHit()
    {
        Destroy();
    }
}
