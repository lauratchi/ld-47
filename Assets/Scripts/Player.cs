﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameEvents.IntEvent onPlayerSetup;
    public GameEvents.IntEvent onPlayerHit;
    public GameEvents.VoidEvent onEndGame;

    [Space(10)]
    public Transform worldCenter;
    public float speed = 5f;

    [Space(10)]
    public int initialHealth = 3;
    public float hitTrauma = 0.4f;

    private float radius;
    private float angle;

    private int health;

    private const float speedMultiplier = 0.001f;

    private void Awake()
    {
        radius = Vector2.Distance(transform.position, worldCenter.position);
        Setup();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            Vector3 scale = transform.localScale;
            scale.y *= -1f;
            transform.localScale = scale;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            Hit();
        }

        float input =Input.GetAxis("Horizontal");

        angle += -input * ((speed * speedMultiplier) + Time.deltaTime);

        Vector3 offset = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0f) * radius;
        transform.position = worldCenter.position + offset;

        Vector3 centerToPlayer = transform.position - worldCenter.position;
        float rotation = Mathf.Atan2(centerToPlayer.y, centerToPlayer.x) * Mathf.Rad2Deg + 90f;
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotation);
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, 0.1f);
    }

    private void Hit()
    {
        health--;

        Game.trauma = Mathf.Min(1f, Game.trauma + hitTrauma);

        if (onPlayerHit != null)
        {
            onPlayerHit.Raise(health);
        }

        if (health == 0)
        {
            if (onEndGame != null)
            {
                onEndGame.Raise();
            }
        }
    }

    public void Setup()
    {
        angle = 180f * Mathf.Deg2Rad;

        health = initialHealth;

        if (onPlayerSetup != null)
        {
            onPlayerSetup.Raise(initialHealth);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Hit();

            BossBullet bullet = collision.gameObject.GetComponent<BossBullet>();
            bullet.PlayerHit();
        }
    }
}
