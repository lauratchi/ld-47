﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEvents
{
    public abstract class BaseGameEventListener<T, E, R> : MonoBehaviour, 
        IGameEventListener<T> where E : BaseGameEvent<T> where R : UnityEvent<T>
    {
        [SerializeField]
        private E gameEvent;

        [SerializeField]
        private R response;

        private void OnEnable()
        {
            if (gameEvent == null) return;

            gameEvent.RegisterListener(this);
        }

        private void OnDisable()
        {
            if (gameEvent == null) return;

            gameEvent.UnregisterListener(this);
        }

        public void OnEventRaised(T parameter)
        {
            if (response != null)
            {
                response.Invoke(parameter);
            }
        }
    }
}
