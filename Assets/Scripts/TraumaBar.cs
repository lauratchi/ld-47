﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TraumaBar : MonoBehaviour
{
    public Image traumaBar;

    private void Update()
    {
        traumaBar.fillAmount = Game.trauma;
    }
}
