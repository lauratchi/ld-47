﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public float traumaDecresingFactor = 1f;

    public static float trauma = 0f;

    private void Update()
    {
        trauma = Mathf.Max(0f, trauma - traumaDecresingFactor * Time.deltaTime);
    }
}
