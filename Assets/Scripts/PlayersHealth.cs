﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersHealth : MonoBehaviour
{
    public GameObject[] healthItems;

    public void OnPlayerSetup(int initialHealth)
    {
        for (int i = 0; i < healthItems.Length; i++)
        {
                healthItems[i].SetActive(i < initialHealth);
        }
    }

    public void OnPlayerHit(int remainingHealth)
    {
        if (remainingHealth >= 0)
        {
            healthItems[remainingHealth].SetActive(false);
        }
    }
}
