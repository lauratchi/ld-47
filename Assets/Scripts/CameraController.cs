﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CameraController : MonoBehaviour
{
    public float maxShakeAngle = 10f;
    public float maxShakeOffset = 0.25f;

    private Vector3 cameraOriginalPosition;
    private Quaternion cameraOriginalRotation;

    private float shake = 0f;

    private void Awake()
    {
        cameraOriginalPosition = transform.position;
        cameraOriginalRotation = transform.rotation;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            shake = Game.trauma * Game.trauma * Game.trauma;
        }

        if (shake > 0f)
        {
            // Rotation
            float angle = maxShakeAngle * shake * Random.Range(-1f, 1f);

            transform.rotation = Quaternion.Euler(cameraOriginalRotation.eulerAngles.x,
                                                  cameraOriginalRotation.eulerAngles.y, 
                                                  cameraOriginalRotation.eulerAngles.z + angle);
            // Translation
            float offsetX = maxShakeOffset * shake * Random.Range(-1f, 1f);
            float offsetY = maxShakeOffset * shake * Random.Range(-1f, 1f);

            transform.position = cameraOriginalPosition + new Vector3(offsetX, offsetY, 0f);

            // Reduce shake
            shake -= Time.deltaTime;
        }
        else
        {
            transform.position = cameraOriginalPosition;
            transform.rotation = cameraOriginalRotation;
        }
    }

    public void OnPlayerHit()
    {
        // Shake
        shake = Game.trauma * Game.trauma;
    }
}
