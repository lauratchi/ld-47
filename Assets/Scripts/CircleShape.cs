﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleShape : MonoBehaviour
{
    public int segments = 64;
    public float radius = 4f;
    public float width = 0.5f;

    private LineRenderer line;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();

        line.positionCount = segments + 1;
        line.startWidth = line.endWidth = width;
        line.useWorldSpace = false;

        CreatePoints();
    }

    private void CreatePoints()
    {
        float x;
        float y;
        float z;

        float angle = 20f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;

            line.SetPosition(i, new Vector3(x, y, 0));

            angle += (360f / segments);
        }
    }
}
